
var gulp = require('gulp'),
    path=require("path"),
    walk=require('walk'),
    fs = require('fs'),
    fse=require('fs.extra'),
    chalk= require('chalk'),
    cheerio =require('cheerio'),
    GitHubApi = require('github');

var canIUseData = require('../lib/props.json');

const USER = 'gbehere';
const REPO = 'https://github5.cisco.com/ccbu-cuic/cuic.git';

const myGit = require('simple-git/promise');
const remote = 'https://github5.cisco.com/ccbu-cuic/cuic.git';

function checkoutGit() {
    myGit().silent(false)
        .clone(remote)
        .then(() => console.log('finished'))
        .catch((err) => console.error('failed: ', err));
}
//checkoutGit();

var sFile={
        "html":["tmpl","html"],
        "css":["css","scss"],
        "js":["js","jsx"]
    },allTsks=[],
    gulpTskJsn = [];

var resultData = {};

var addGlpTsk = (tskNm, typ, file) => {
    let myRand = tskNm + "-----------" + typ + "----------" + (Math.floor(Math.random() * 1000));
    //console.log(file);

    allTsks.push(myRand);
    // gulp.task(myRand, function (done) {
    //    gulpTskJsn.push(myRand)
    resultData[file] = {
        keywords: []
    };
    var data = fs.readFileSync(file);
    if (data) {
        if (typ === 'js') {
            //console.log(canIUseData.js);
            canIUseData.js.forEach(function (keyword) {
                if (data.indexOf(keyword) != -1) {
                    console.log('found: ', file, ' ', keyword);
                    resultData[file].keywords.push(keyword);
                }
            });
        }
    }
    //done(true);
    //});
};

var checkArg = function (arg) {
    return arg['search'];
};

module.exports = {
    triverceDir: (arg, isApi,soc)=>{
        sFile.htmlA = [];
        sFile.cssA = [];
        sFile.jsA = [];
        var argIsValid = checkArg(arg);
        if(argIsValid){
            let files   = [];
            if(isApi){
                soc.emit('ana-resp',{msg:'Serchinh in  '+ (JSON.stringify(arg['search'],null,4)) +' is not valid'});
            }else {
                console.log('Serchinh in  '+ (JSON.stringify(arg['search'],null,4)) +' is not valid')
            }
        let walker  = walk.walk(arg['search'], { followLinks: false });

        walker.on('file', (root, stat, next)=>{
            let typ=stat.name.split(".");
            let filS = typ[typ.length-1].toLowerCase();
            let fl = root + '/' + stat.name;
            fl.replace(/\\\\/g, '\\');
            if(sFile.html.includes(filS) && !arg.skipDir.test(fl)){
                sFile.htmlA.push(fl);
                if(isApi) soc.emit('ana-resp',{msg:fl});
            }else if(sFile.css.includes(filS) && !arg.skipDir.test(fl)){
                sFile.cssA.push(fl);
                if(isApi) soc.emit('ana-resp',{msg:fl});
            }else if(sFile.js.includes(filS) && !arg.skipDir.test(fl)){
                sFile.jsA.push(fl);
                if(isApi) soc.emit('ana-resp',{msg:fl});
            }
            next();
        });
        walker.on('end', function() {
            console.log(sFile);
            sFile.jsA.forEach(o=>{
                let tskNm=(o.substring(o.lastIndexOf("\\")+1,o.lastIndexOf("."))+Math.floor(Math.random()*1000)).replace(/[^a-zA-Z0-9_]/g, "");
                addGlpTsk(tskNm,'js',o);
            });
            sFile.cssA.forEach(o=>{
                let tskNm=(o.substring(o.lastIndexOf("\\")+1,o.lastIndexOf("."))+Math.floor(Math.random()*1000)).replace(/[^a-zA-Z0-9_]/g, "");
                addGlpTsk(tskNm,'css',o);
            });
            sFile.htmlA.forEach(o=>{
                let tskNm=(o.substring(o.lastIndexOf("\\")+1,o.lastIndexOf("."))+Math.floor(Math.random()*1000)).replace(/[^a-zA-Z0-9_]/g, "");
                addGlpTsk(tskNm,'html',o);
            });
            fs.writeFile('../out/allTsks.json', JSON.stringify(allTsks, null, 4), function (err, data) {
                console.log(chalk.green("triverceDir is done"));
            });
            fs.writeFile('../out/result.json', JSON.stringify(resultData, null, 4), function (err, data) {
                console.log(err);
            });
            fs.writeFile('../out/gulpTskJsn.json', JSON.stringify(gulpTskJsn, null, 4), function (err, data) {
                fs.writeFile('../out/sFile.json', JSON.stringify(sFile, null, 4), function (err, data) {
                    console.log(chalk.green("triverceDir is done"));
                });
                // gulp.task('triverceDir', allTsks, () => {

                //     });
            });
            //gulp.start('triverceDir');
        });
        }else {
            if(isApi){
                soc.emit('ana-resp',{msg:'Arg '+ (JSON.stringify(arg,null,4)) +' is not valid'});
            }else {
                console.log('Arg '+ (JSON.stringify(arg,null,4)) +' is not valid')
            }
        }
    }
};