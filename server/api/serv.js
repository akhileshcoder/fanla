var express = require('express'),
    app = express(),
    m = require('./common/middleware.js'),
    analyse=require("./controllers/analyse");


var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var soc = io.sockets;

server.listen(8999, function() {
    console.log('listening on port : ' ,8999);
});

m.middleware(app,express);



var activeClients = 0;
var sockets = {};
soc.on('connection', function(socket){clientConnect(socket)});

function clientConnect(socket){
    activeClients++;
    soc.emit('message', {clients:activeClients});
    socket.on('disconnect', function(){clientDisconnect(socket)});

    socket.on('analyse_git', function(msg){
        analyse.git(msg,soc);
    });
    socket.on('analyse_local', function(msg){
        analyse.local(msg,soc);
    });
}

function clientDisconnect(socket){
    activeClients--;
    //activeClients=Object.keys(sockets).length;
    soc.emit('message', {clients:activeClients});
    delete sockets[socket.id];
}