const bodyParser = require('body-parser'),
	cors = require('cors');
module.exports.middleware = function(app,express) {
	app.use(cors());
	app.use(bodyParser.json({limit: '5mb'}));
	app.use(bodyParser.urlencoded({extended: true,limit: '5mb'}));
	app.use(express.static(__dirname + '/../../../public/dist'));
	app.use('/', function (req, res, next) {
		console.log("req in route");
	  next();
	});
};