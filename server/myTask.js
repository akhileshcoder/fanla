/**
 * Created by akhilesh.kumar on 8/4/2016.
 */
var chalk= require('chalk'),
    ana = require('./utill/analise');

var argCmd =process.argv.slice(2),
    arg={},
    strRegStr="",
    skipDir="",
    strReg;


var createArg=()=>{
    console.log(chalk.cyan("\n          _              _         _             _             _       \n        /\\ \\            /\\ \\      / /\\         /\\ \\           /\\ \\     \n       /  \\ \\           \\ \\ \\    / /  \\       /  \\ \\         /  \\ \\    \n      / /\\ \\ \\          /\\ \\_\\  / / /\\ \\__   / /\\ \\ \\       / /\\ \\ \\   \n     / / /\\ \\ \\        / /\\/_/ / / /\\ \\___\\ / / /\\ \\ \\     / / /\\ \\ \\  \n    / / /  \\ \\_\\      / / /    \\ \\ \\ \\/___// / /  \\ \\_\\   / / /  \\ \\_\\ \n   / / /    \\/_/     / / /      \\ \\ \\     / / /    \\/_/  / / /   / / / \n  / / /             / / /   _    \\ \\ \\   / / /          / / /   / / /  \n / / /________  ___/ / /__ /_/\\__/ / /  / / /________  / / /___/ / /   \n/ / /_________\\/\\__\\/_/___\\\\ \\/___/ /  / / /_________\\/ / /____\\/ /    \n\\/____________/\\/_________/ \\_____\\/   \\/____________/\\/_________/     \n                                                                       \n\n        _      _        _         _          _            _           _   _       \n       / /\\   /\\ \\     /\\_\\      / /\\       /\\ \\         /\\ \\        /\\_\\/\\_\\ _   \n      / /  \\  \\ \\ \\   / / /     / /  \\      \\_\\ \\       /  \\ \\      / / / / //\\_\\ \n     / / /\\ \\__\\ \\ \\_/ / /     / / /\\ \\__   /\\__ \\     / /\\ \\ \\    /\\ \\/ \\ \\/ / / \n    / / /\\ \\___\\\\ \\___/ /     / / /\\ \\___\\ / /_ \\ \\   / / /\\ \\_\\  /  \\____\\__/ /  \n    \\ \\ \\ \\/___/ \\ \\ \\_/      \\ \\ \\ \\/___// / /\\ \\ \\ / /_/_ \\/_/ / /\\/________/   \n     \\ \\ \\        \\ \\ \\        \\ \\ \\     / / /  \\/_// /____/\\   / / /\\/_// / /    \n _    \\ \\ \\        \\ \\ \\   _    \\ \\ \\   / / /      / /\\____\\/  / / /    / / /     \n/_/\\__/ / /         \\ \\ \\ /_/\\__/ / /  / / /      / / /______ / / /    / / /      \n\\ \\/___/ /           \\ \\_\\\\ \\/___/ /  /_/ /      / / /_______\\\\/_/    / / /       \n \\_____\\/             \\/_/ \\_____\\/   \\_\\/       \\/__________/        \\/_/        \n                                                                                  \n"));
    argCmd.forEach(o=>{
        let prm=o.split("=");
        if(prm[0].indexOf("--")===0){
            if(prm[0].indexOf("vendor")!==2){
                arg[prm[0].substring(2)]=prm[1]
            }else if(prm[0].indexOf("vendor")===2) {
                strRegStr+=prm[1]+"|";
            }
        }else {
            strRegStr+=prm[0]+"|";
        }
    });
    if(!strRegStr){
        strRegStr="node_modules|bower_components|target|build|dist"
    }
    strReg=new RegExp(strRegStr);
    arg.skipDir = strReg;
};

var runScrpt=()=>{
    console.log(chalk.green("arg are: ",JSON.stringify(arg,null,4)));
    if(arg['option']==='traverse'){
        ana.triverceDir(arg, false)
    }else {
        console.log("arg['option']=",arg['option'])
    }
};
createArg();
runScrpt();
