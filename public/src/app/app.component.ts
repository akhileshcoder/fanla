import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  isReportView=false;
  msgArr = {t:[],h:[]};
  cardVisible=[true];
  data:any = {
    search:"/Users/akhilesk/Desktop/fAnl/server/testDir",
    vendor:"node_modules|bower_components|target|build|dist"
  };
  socket:any;
  visibleDrop:any;
  scanReport:any = [{
    name:"/Users/akhilesk/Desktop/fAnl/server/testDir/test.js",
    keywords: [
      {
        name:"Object.defineProperty",
        lines:[[20,5],[30,5]],
        browser:[{
          name:"opera",
          version:["1.5","2.3"]
        }]
      },
      {
        name:"const",
        lines:[[15,5],[30,5]],
        browser:[{
          name:"opera",
          version:["1.5","2.3"]
        }]
      },
      {
        name:"let",
        lines:[[20,5],[30,5]],
        browser:[{
          name:"opera",
          version:["1.5","2.3"]
        }]
      }
    ]
  },
    {
      name:"/Users/akhilesk/Desktop/fAnl/server/testDir/test.js",
      keywords: [
        {
          name:"Object.defineProperty",
          lines:[[20,5],[30,5]],
          browser:[{
            name:"opera",
            version:["1.5","2.3"]
          }]
        },
        {
          name:"const",
          lines:[[15,5],[30,5]],
          browser:[{
            name:"opera",
            version:["1.5","2.3"]
          }]
        },
        {
          name:"let",
          lines:[[20,5],[30,5]],
          browser:[{
            name:"opera",
            version:["1.5","2.3"]
          }]
        }
      ]
    }];
  imgNameMap = {
    "ie": "assets/img/ie.png",
    "edge": "assets/img/edge.png",
    "firefox": "assets/img/firefox.jpeg",
    "chrome": "assets/img/chrome.jpeg",
    "safari": "assets/img/safari.jpeg",
    "opera": "assets/img/opera.jpeg",
    "ios_saf": "assets/img/iossafari.png",
    "op_mini": "assets/img/opera-mini.jpeg",
    "android": "assets/img/android.jpeg",
    "bb": "assets/img/bb.jpeg",
    "op_mob": "assets/img/operaMob.png",
    "and_chr": "assets/img/android-chrome.jpeg",
    "and_ff": "assets/img/androidFF.jpeg",
    "ie_mob": "assets/img/androidIE.png",
    "and_uc": "assets/img/androUC.jpeg",
    "samsung": "assets/img/samsung.jpeg",
    "and_qq": "assets/img/qq.jpeg",
    "baidu": "assets/img/baidu.jpeg"
  };
  suportedBrower = {
    "ie": {
      "name": "IE",
      "img": "assets/img/ie.png",
      "selectedV": [],
      "v": [
        "11","10","9","8","7","6"
      ]
    },
    "edge": {
      "name": "Edge",
      "img": "assets/img/edge.png",
      "selectedV": [],
      "v": [
        "17","16","15","14","13","12"
      ]
    },
    "firefox": {
      "name": "Firefox",
      "img": "assets/img/firefox.jpeg",
      "selectedV": [],
      "v": [
        "60","59","58","57","56","55","54","53","52","51","50","49","48","47","46","45","44","43","42","41",
        "40","39","38","37","36","35","34","33","32","31","30","29","28","27","26","25","24","23","22","21","20",
        "19","18","17","16","15","14","13","12","11","10","9","8","7","6","5","4","3.6","3.5","3","2"
      ]
    },
    "chrome": {
      "name": "Chrome",
      "img": "assets/img/chrome.jpeg",
      "selectedV": [],
      "v": [
        "66","65","64","63","62","61","60","59","58","57","56","55","54","53","52","51","50","49","48","47","46",
        "45","44","43","42","41","40","39","38","37","36","35","34","33","32","31","30","29","28","27","26","25",
        "24","23","22","21","20","19","18","17","16","15","14","13","12","11","10","9","8","7","6","5","4"
      ]
    },
    "safari": {
      "name": "Safari",
      "img": "assets/img/safari.jpeg",
      "selectedV": [],
      "v": [
        "TP","11","10.1","10","9.1","9","8","7.1","7","6.1","6","5.1","5","4","3.2","3.1"
      ]
    },
    "opera": {
      "name": "Opera",
      "img": "assets/img/opera.jpeg",
      "selectedV": [],
      "v": [
        "50","49","48","47","46","45","44","43","42","41","40","39","38","37","36","35","34","33","32","31","30",
        "29","28","27","26","25","24","23","22","21","20","19","18","17","16","15","12.1","11.5","10.0-10.1"
      ]
    },
    "ios_saf": {
      "name": "IOS Safari",
      "img": "assets/img/iossafari.png",
      "selectedV": [],
      "v": [
        "11.0-11.2","10.3","10.0-10.2","9.3","9.0-9.2","8.1-8.4","8","7.0-7.1","6.0-6.1","5.0-5.1","4.2-4.3","4.0-4.1","3.2"
      ]
    },
    "op_mini": {
      "name": "Opera Mini",
      "img": "assets/img/opera-mini.jpeg",
      "selectedV": [],
      "v": [
        "all"
      ]
    },
    "android": {
      "name": "Android Browser",
      "img": "assets/img/android.jpeg",
      "selectedV": [],
      "v": [
        "62","4.4.3-4.4.4","4.4","4.2-4.3","4.1","4","3","2.3","2.2","2.1"
      ]
    },
    "bb": {
      "name": "Black barry",
      "img": "assets/img/bb.jpeg",
      "selectedV": [],
      "v": [
        "10","7"
      ]
    },
    "op_mob": {
      "name": "Opera mobile",
      "img": "assets/img/operaMob.png",
      "selectedV": [],
      "v": [
        "37","12.1","12"
      ]
    },
    "and_chr": {
      "name": "Android Chrome",
      "img": "assets/img/android-chrome.jpeg",
      "selectedV": [],
      "v": [
        "62"
      ]
    },
    "and_ff": {
      "name": "Android Firefox",
      "img": "assets/img/androidFF.jpeg",
      "selectedV": [],
      "v": [
        "57"
      ]
    },
    "ie_mob": {
      "name": "Mobile IE",
      "img": "assets/img/androidIE.png",
      "selectedV": [],
      "v": [
        "11","10"
      ]
    },
    "and_uc": {
      "name": "Mobile UC",
      "img": "assets/img/androUC.jpeg",
      "selectedV": [],
      "v": [
        "11.4"
      ]
    },
    "samsung": {
      "name": "Samsung",
      "img": "assets/img/samsung.jpeg",
      "selectedV": [],
      "v": [
        "6.2","5","4"
      ]
    },
    "and_qq": {
      "name": "QQ browser",
      "img": "assets/img/qq.jpeg",
      "selectedV": [],
      "v": [
        "1.2"
      ]
    },
    "baidu": {
      "name": "Baidu",
      "img": "assets/img/baidu.jpeg",
      "selectedV": [],
      "v": [
        "7.12"
      ]
    }
  };
  suportedBrowerA = [];
  ngAfterViewInit(){
    setTimeout(()=>{
      let locDta = this.getLocalData();
      if(!locDta[2]) {
        this.suportedBrowerA = Object.keys(this.suportedBrower).map(i => {
          this.suportedBrower[i].selectedV = this.suportedBrower[i].v.slice(0,3);
          this.suportedBrower[i].key = i;
          return this.suportedBrower[i];
        });
      }
      document.body.addEventListener('click',(event)=>{
        let itm = (<any> event.target).getAttribute('class');
        if(!itm || itm.indexOf('drop-true')===-1){
          this.visibleDrop = null;
        }
      })
    },10);
    this.socket = (<any> window).io.connect('http://localhost:8999');
    this.socket.on('message', msg=>{
      document.getElementById('active-client').innerText = 'No. of active client:'+msg.clients;
    });
  }

  showDrop(ev,ind){
    if(true){
      this.visibleDrop = ind;
    }
  }
  pushRmvVersion(isPush,v,ind){
    if(isPush){
      if(this.suportedBrowerA[ind].selectedV.indexOf(v)===-1){
        this.suportedBrowerA[ind].selectedV.push(v);
      }
    }else {
      this.suportedBrowerA[ind].selectedV.splice(this.suportedBrowerA[ind].selectedV.indexOf(v),1);
    }
    this.saveLocalDta();
  }

  rendorNMsg(n,end) {
    if(this.msgArr.t.length){
      this.msgArr.h = this.msgArr.t.slice(end-n>=0?end-n:0,end);
    }
    setTimeout(()=>{
      let slc = document.querySelector("#process-status");
      slc.scrollTop = slc.scrollHeight
    },100)
  }
  analyse(typ) {
    this.cardVisible = [];
    this.cardVisible[4]=true;
    this.data.suportedBrowerA = this.suportedBrowerA;
    this.socket.emit(typ ==='git'?'analyse_git':'analyse_local',this.data);
    this.socket.on('ana-resp', msg=>{
      this.msgArr.t.push('<pre class="mb-0 mt-0">'+ msg.msg+'</pre>');
      this.rendorNMsg(100,this.msgArr.t.length-1);
    });
    this.saveLocalDta();
  }

  saveLocalDta(){
    localStorage.setItem('cardVisible',JSON.stringify(this.cardVisible));
    localStorage.setItem('data',JSON.stringify(this.data));
    localStorage.setItem('suportedBrowerA',JSON.stringify(this.suportedBrowerA));
  }
  getLocalData(){
    let cardVisible = localStorage.getItem('cardVisible');
    let data = localStorage.getItem('data');
    let suportedBrowerA = localStorage.getItem('suportedBrowerA');
    if(cardVisible){
      this.cardVisible = JSON.parse(cardVisible);
    }
    if(data){
      this.data = JSON.parse(data);
    }
    if(suportedBrowerA){
      this.suportedBrowerA = JSON.parse(suportedBrowerA);
    }
    return [cardVisible,data,suportedBrowerA]
  }
}
